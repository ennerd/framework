<?php
namespace The\Framework;

class ClassAlreadyRegisteredException extends Exception {

	public $className;

	public function __construct( $className ) {
		$this->className = $className;
		parent::__construct('The class '.$className.' is already registered.');
	}

}
