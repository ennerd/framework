<?php
namespace The\Framework;

class The {

	/**
	 * Add a class to The\ namespace.
	 */
	public static function addClass( $className, $yourClassName ) {
		class_alias( $yourClassName, $className );
		unset($_autoloadMap[$className]);
	}

	/**
	 * Registers an autoloader for your class, which will be invoked unless an earlier
	 * autoloader have already loaded the class.
	 * This is mostly used by the default implementations.
	 */
	public static function setDefaultClass( $className, $yourClassName ) {
		if(static::$_autoloadMap === null) {
			// We must create an autoloader for the first default class and the ones to follow.
			$theAutoloader = function($requestedClass) {
				if(isset(static::$_autoloadMap[$requestedClass])) {
					class_alias( static::$_autoloadMap[$requestedClass], $requestedClass );
					return true;
				}
				return false;
			};
			spl_autoload_register($theAutoloader);
			static::$_autoloadMap = [];
		};
		static::$_autoloadMap[$className] = $yourClassName;
	}

	protected static $_autoloadMap;
}
